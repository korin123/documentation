* [Documentation Home](README.md)  
  * [Getting Started With Integration](getting-started.md)  
# Project configuration

To create a Project:

Sign in to the [Console](https://console.geouniq.com) using your account's credentials.
Add your first Project, give it a name of your choise, and (optionally) a description.
Copy the [API Key](service-architecture.md#projects-and-client-apps) generated by the system and keep it safe. You need it to perform requests to our Web API.
Now, you are ready to configure your project with the mobile apps for which you want to use GeoUniq. For each of them, add a Client App to your Project.

> A Project can be configured with any number of Client Apps, both Android and iOS. Devices of different Client Apps within the same Project will constitute a unique "device-base". You will be able to get geolocation analytics related to all your device-base. Alternatively, you can create different Projects for each mobile app or create any configuration you wish. It is all up to you and your requirements.

To add a Client App to a Project:

* Enter the Project by clicking on the Project's name.
* Click the "add" button on the Android or iOS box depending on the mobile app's OS of the specific app you want to add.
* For Android apps, provide the app's package name and the SHA1 fingerprint of the app's certificate. For iOS apps, provide the app's Bundle ID. Then Submit to create the Client App.
* Copy the Mobile Key generated by the system and keep it safe. You will need it when configuring your mobile app to work with GeoUniq SDK.

[Next: SDK Integration](sdk/integration/index.md)
