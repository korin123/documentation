* [Documentation Home](../../README.md)
   * [Web API](../index.md)

# Use Cases

* General aspects
    * [Obtain an access token](obtain-access-token.md)
    * [Look at the trend of device registrations](various/device-registrations-trend.md)
* [Get the location history of a device](various/location_history.md)
* [Finding devices that visited an area](various/visiting-devices.md)
* [Count devices in a place](various/num_devices_in_a_place.md)
* [Triggers](triggers/index.md)
* [Home and Work Location](home-work/index.md)
* [User Profile](profile/index.md)
* [Audiences](audience/index.md)
* [Custom POIs](custom-pois.md)
* [Receiving WebHooks](receiving-web-hooks.md)




