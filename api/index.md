* [Documentation Home](../README.md)

# Web API
* [Overview](overview.md)
* [Basic concepts](concepts/index.md)
    * [Resources and their definition](concepts/resource-definition.md)
    * [Selection of Resources](concepts/resource-selection.md)
    * [Statistics](concepts/statistics.md)
* [Use Cases](use-cases/index.md)
* [Reference documentation](reference/index.md)
