* [Documentation Home](../../../../../README.md)  
  * [Web API](../../../../index.md)  
    * [Reference](../../../index.md)
        * [Resources](../../index.md)
           * [Platform-created Resources](../index.md)
                * [Device-related Resources](index.md)

# UserProfile

A *UserProfile* represents the set of sociodemographic attributes inferred for an individual 
(identified through a *Device*) by observing its geo-behaviour.

## Metrics

No metric is defined for *UserProfile*

## Data Model

The data model used to represent a *UserProfile* is 
[`UserProfile`](../../../data-models/resources/platform-created/device-related/profile.md).

## Endpoints

- [Resources](../../../endpoints/resources/platform-created/device-related/profile.md)

