* [Documentation Home](../../../../../README.md)  
  * [Web API](../../../../index.md)  
    * [Reference](../../../index.md)
        * [Resources](../../index.md)
           * [Platform-created Resources](../index.md)
           
# Device-related Resources

This section lists the various Device-related resource types defined by this API.  
A Device-related resource models some information that is related to a specific [*Device*](../../../resources/device-related/device.md).

## List of Device-related Resource types

* [*Position*](position.md)
* [*Visit*](visit.md)
* [*Home Location*](home-location.md)
* [*Work Location*](work-location.md)
* [*User Profile*](profile.md)
* [*Nightlife Index*](nightlife-index.md)
* [*Tracking Summary*](tracking-summary.md)
