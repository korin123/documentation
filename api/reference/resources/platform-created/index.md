* [Documentation Home](../../../../README.md)  
  * [Web API](../../../index.md)  
    * [Reference](../../index.md)
       * [Resources](../index.md)
    
# Platform-created Resources

This section lists the various [platform-created resource types](../../../concepts/resource-definition.md) 
defined by this API.  
Platform-created resources are automatically created by GeoUniq Analytics platform.

Each sub-section provides a general description of the resource type, explaining what it represents from a logical point of view.  
In addition, for each resource type *R*, the following is indicated.

- **[Attributes](../../../concepts/resource-definition.md#resource-attributes)**   
The set of attributes defined for the resource, along with the 
[dimension](../../../concepts/resource-definition.md#dimensions) and 
[configuration](../../../concepts/resource-definition.md#configuration-of-a-resource-attribute)
of each of them.

- **[Metrics](../../../concepts/resource-definition.md#metrics)**   
The set of metrics defined for the resource.

- **[Resource Data Model](../../data-models/resources/platform-created/index.md)**  
The data model used to represent the resource type *R*.

- **[Resource Selection Data Model](../../data-models/r-selection/index.md)**  
The data model used to indicate an [*R* Selection](../../../concepts/resource-selection.md).

- **[API Endpoints](../../endpoints/resources/platform-created/index.md)**.  
Links to the endpoints related to *R*.

## List of Platform-created Resource types

* [*Device*](device.md)
* [Device-related Resources](device-related/index.md)
    * [*Position*](device-related/position.md)
    * [*Visit*](device-related/visit.md)
    * [*Home Location*](device-related/home-location.md)
    * [*Work Location*](device-related/work-location.md)
    * [*User Profile*](device-related/profile.md)
    * [*Nightlife Index*](device-related/nightlife-index.md)
    * [*Tracking Summary*](device-related/tracking-summary.md)
* [Trigger Log](triggerlog.md)
* [POI](poi.md)
    
