* [Documentation Home](../../../../README.md)  
  * [Web API](../../../index.md)  
    * [Reference](../../index.md)
        * [Resources](../index.md)
           * [Platform-created Resources](index.md)

# TriggerLog

A *TriggerLog* models a single occurrence of an event defined by a [*Trigger*](../user-created/trigger.md).
*Triggerlog* resources are automatically created by GeoUniq analytics platform 
each time the event defined by a specific *Trigger* is detected.

## Resource Data Model

The data model used to represent a *TriggerLog* is [`Triggerlog`](../../data-models/resources/platform-created/triggerlog.md)

## Endpoints

- [Selection](../../endpoints/platform-created/triggerlog/selection.md)
- [Statistic](../../endpoints/platform-created/triggerlog/statistic.md)


