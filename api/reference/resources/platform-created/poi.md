* [Documentation Home](../../../../README.md)  
  * [Web API](../../../index.md)  
    * [Reference](../../index.md)
        * [Resources](../index.md)
           * [Platform-created Resources](index.md)
# POI

A *POI* (Point Of Interest) relates to a specific venue located somewhere that can be used to implicitely indicate a geographical location.

## Metrics

No metric is defined for *POI*

## Resource Data Model

The data model used to represent a *POI* is [`POI`](../../data-models/resources/platform-created/poi.md)

## Resource Segment Data Model

The data model used to represent a *POI* Segment is [`POI-Segment`](../../data-models/r-selection/poi.md)

## Endpoints

- [Selection](../../endpoints/platform-created/poi/selection.md)
- [Statistic](../../endpoints/platform-created/poi/statistic.md)

