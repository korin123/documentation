* [Documentation Home](../../../README.md)  
  * [Web API](../../index.md)  
    * [Reference](../index.md)
    
# Resources

This section lists the various [*resource types*](../../concepts/resource-definition.md) defined by this API.
Each *Resource type* is documented in its own sub-section.

This section is divided in two main sub-sections

* [Platform-created resource types](platform-created/index.md)
    * [*Device*](platform-created/device.md)
    * [Device-related Resources](platform-created/device-related/index.md)
        * [*Position*](platform-created/device-related/position.md)
        * [*Visit*](platform-created/device-related/visit.md)
        * [*HomeLocation*](platform-created/device-related/home-location.md)
        * [*WorkLocation*](platform-created/device-related/work-location.md)
        * [*UserProfile*](platform-created/device-related/profile.md)
        * [*NightLifeIndex*](platform-created/device-related/profile.md)
        * [*TrackingSummary*](platform-created/device-related/tracking-summary.md)
    * [TriggerLog](platform-created/triggerlog.md)
    * [POI](platform-created/poi.md)
        
* [User-created resource types](user-created/index.md)
    * [*Campaign*](user-created/campaign.md)
    * [*Trigger*](user-created/trigger.md)
    * [*Batch Analysis*](user-created/batch-analysis.md)
        * [*Insights Analysis*](user-created/insights-analysis.md)
        * [*Audience Analysis*](user-created/audience-analysis.md)
        * [*Footfall Analysis*](user-created/campaign-footfall.md)
