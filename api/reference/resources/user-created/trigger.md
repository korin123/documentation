* [Documentation Home](../../../../README.md)  
  * [Web API](../../../index.md)  
    * [Reference](../../index.md)
        * [Resources](../index.md)
           * [User-created Resources](index.md)
        
# Trigger

A *Trigger* resource models an event that has to be monitored for devices.

## Resource Data Model

The data model used to represent a *Trigger* is [`Trigger`](../../data-models/resources/user-created/trigger.md)

## Endpoints

- [Create Resource](../../endpoints/user-created/trigger/create.md)
- [Read Collection](../../endpoints/user-created/trigger/read-collection.md)
- [Read Resource](../../endpoints/user-created/trigger/read-resource.md)
- [Update Resource](../../endpoints/user-created/trigger/update.md)
- [Delete Resource](../../endpoints/user-created/trigger/delete.md)

