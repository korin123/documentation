* [Documentation Home](../../../../README.md)  
  * [Web API](../../../index.md)  
    * [Reference](../../index.md)
        * [Resources](../index.md)
        
# User-created Resources

This section lists the various User-created Resource types defined by this API.
A User-created Resource type models some information that is explicitly created by a User of the API.

## List of User-created Resource types

* [*Campaign*](campaign.md)
* [*Trigger*](trigger.md)
* [*Batch Analysis*](batch-analysis.md)
    * [*Insights Analysis*](insights-analysis.md)
    * [*Audience Analysis*](audience-analysis.md)
    * [*Footfall Analysis*](campaign-footfall.md)
