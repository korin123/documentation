* [Documentation Home](../../README.md)  
  * [Web API](../index.md)

# Web API Reference Documentation 
This section provides a reference documentation for developers

# Index

* [General Aspects](general-aspects/index.md)
    * [Content Type](general-aspects/content-type.md)
    * [Pagination](general-aspects/pagination.md)
    * [Authentication and Authorization](general-aspects/auth.md)
* [Dimensions](dimensions/index.md)
* [Configurations and Selection Operators](configurations-and-operators.md)
* [Resources](resources/index.md)
* [Data Models](data-models/index.md)
* [Endpoints](endpoints/index.md)

