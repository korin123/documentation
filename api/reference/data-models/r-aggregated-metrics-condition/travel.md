
# Travel-AggregatedMetricsCondition

Name        |Type      
------------|----------
count | [`ComparisonCondition`](/api/reference/data-modelsata-models/common/comparison-condition.md) 
totalTravelledDistance | [`ComparisonCondition`](/api/reference/data-modelsata-models/common/comparison-condition.md) 
totalDuration | [`ComparisonCondition`](/api/reference/data-modelsata-models/common/comparison-condition.md) 
averageTravelledDistance | [`ComparisonCondition`](/api/reference/data-modelsata-models/common/comparison-condition.md) 
averageDuration | [`ComparisonCondition`](/api/reference/data-modelsata-models/common/comparison-condition.md) 
averageSpeed | [`ComparisonCondition`](/api/reference/data-modelsata-models/common/comparison-condition.md) 
averageStartEndDistance | [`ComparisonCondition`](/api/reference/data-modelsata-models/common/comparison-condition.md) 
averageStraightIndex | [`ComparisonCondition`](/api/reference/data-modelsata-models/common/comparison-condition.md) 
averageStationaryIndex | [`ComparisonCondition`](/api/reference/data-modelsata-models/common/comparison-condition.md) 

```json
{
    "count": {
        "gt": 10,
        "lt": 20
    },
    "totalTravelledDistance": {},
    "totalDuration": {},
    "averageTravelledDistance": {},
    "averageDuration": {},
    "averageStartEndDistance": {},
    "averageSpeed": {},
    "averageStraightIndex": {},
    "averageStationaryIndex": {}
}
```
