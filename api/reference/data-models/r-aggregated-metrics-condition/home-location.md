
# HomeLocation-AggregatedMetricsCondition

Name        |Type      
------------|----------
count |[`ComparisonCondition`](/api/reference/data-modelsata-models/common/comparison-condition.md) 

```json
{
    "count": {
        "gt": 10,
        "lt": 20
    }
}
```
