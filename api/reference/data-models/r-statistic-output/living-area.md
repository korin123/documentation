# LivingArea-StatisticOutput

Name        |Type      | Description
------------|----------|------------
segment | [`LivingArea-Segment`](/api/reference/data-modelsata-models/r-segment/living-area.md) | Indicates which Segment of Living Areas the item refers to. This field is not provided if no Segmentation as been requested.
metrics | [`LivingArea-AggregatedMetrics`](/api/reference/data-modelsata-models/r-aggregated-metrics/living-area.md)  | Contains the aggregated metrics computed for the Statistic on Living Areas.

