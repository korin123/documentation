# Travel-StatisticOutput

Name        |Type      | Description
------------|----------|------------
segment | [`Travel-Segment`](/api/reference/data-modelsata-models/r-segment/travel.md) | Indicates which Segment of Travels the item refers to. This field is not provided if no Segmentation as been requested.
metrics | [`Travel-AggregatedMetrics`](/api/reference/data-models/r-aggreated-metrics/travel.md)  | Contains the aggregated metrics computed for the Statistic on Travels.

