# HomeLocation-StatisticOutput

Name        |Type      | Description
------------|----------|------------
segment | [`HomeLocation-Segment`](/api/reference/data-modelsata-models/r-segment/home-location.md) | Indicates which Segment of Home Locations the item refers to. This field is not provided if no Segmentation as been requested.
metrics | [`HomeLocation-AggregatedMetrics`](/api/reference/data-modelsata-models/r-aggregated-metrics/home-location.md)  | Contains the aggregated metrics computed for the Statistic on Home Location.

