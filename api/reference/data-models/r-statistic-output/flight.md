# Flight-StatisticOutput

Name        |Type      | Description
------------|----------|------------
segment | [`Flight-Segment`](/api/reference/data-modelsata-models/r-segment/flight.md) | Indicates which Segment of Flights the item refers to. This field is not provided if no Segmentation as been requested.
metrics | [`Flight-AggregatedMetrics`](/api/reference/data-modelsata-models/r-aggregated-metrics/flight.md)  | Contains the aggregated metrics computed for the Statistic on Flights.
