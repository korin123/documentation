# WorkLocation-StatisticOutput

Name        |Type      | Description
------------|----------|------------
segment | [`WorkLocation-Segment`](/api/reference/data-modelsata-models/r-segment/work-location.md) | Indicates which Segment of Work Locations the item refers to. This field is not provided if no Segmentation as been requested.
metrics | [`WorkLocation-AggregatedMetrics`](/api/reference/data-modelsata-models/r-aggregated-metrics/work-location.md)  | Contains the aggregated metrics computed for the Statistic on Work Location.

