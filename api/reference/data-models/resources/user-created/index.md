# User-created Resources


Partial data model common to all [User-created](/api/concepts/resource-definition.md) Resources.

Name        | Type      | Mandatory/Optional | Allowed Values | Description
------------|----------|----------------|-----------|--------------
id | `String` | Forbidden - Output Only | N.A. | The Resource ID.
createdAt | [`Date`](/api/reference/data-modelsata-models/common/date.md) | Forbidden - Output Only | N.A. | The date and time when the Resource was created.
name | `String` | Optional | Any `String` of length lower than 32 characters | A name for the Resource useful to recall it in a [`UserCreatedResource-Segment`](/api/reference/data-models/common/user-created-resource-segment.md).
description | `String` | Optional | Any `String` of length lower than 128 characters | A description for the Resource.
labels | `[String]` | Optional | At most 10 `String` elements of length lower than 16 characters | A set of labels useful to recall the Resource in a [`UserCreatedResource-Segment`](/api/reference/data-models/common/user-created-resource-segment.md).

```json
{
    "id":"resource1",
    "name":"my resource",
    "description": "my first resource",
    "labels": ["test"]
}
```

According to the set of defined [User-created Resources](/api/reference/resources/resources/user-created/index.md), the following Data Models are defined.

* [Trigger](/api/reference/data-modelsata-models/resources/user-created/trigger.md)
* [Campaign](/api/reference/data-modelsata-models/resources/user-created/campaign.md)
* [Batch Analysis](/api/reference/data-modelsata-models/resources/user-created/batch-analysis.md)
    * [Insight Analysis](/api/reference/data-models/resources/user-created/insights-analysis.md)
    * [Audience Analysis](/api/reference/data-modelsata-models/resources/user-created/audience-analysis.md)
    * [Footfall Analysis](/api/reference/data-modelsata-models/resources/user-created/campaign-footfall.md)

