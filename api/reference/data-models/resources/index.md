# Resources Data Models

This section lists Data Models for Resources.

This Data Models are used:


- **on the input side**:
    - [**when creating or modifying User-created Resources**](/api/reference/endpoints/endpoints/resources/user-created/index.md)

- **on the output side**:
    - [**when accessing Resources**](/api/reference/endpoints/endpoints/resources/index.md), both User-created and Platform-created


The various Resources Data Models are divided into:

- [Platform-created](/api/reference/data-modelsata-models/resources/platform-created/index.md): Data Models for Platform-created Resource types
- [User-created](/api/reference/data-modelsata-models/resources/user-created/index.md): Data Models for User-created Resource types
