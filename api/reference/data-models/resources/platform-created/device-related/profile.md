# UserProfile

Data Model used to represent a [*UserProfile*](/api/reference/resources/resources/platform-created/device-related/profile.md) Resource.

## Fields Specificcation

Name        |Type      | Possible Values |  Description
------------|----------|----------------|-----------
device | [`DeviceReference`](/api/reference/data-modelsata-models/common/device-reference.md) | ANY valid `DeviceReference` | Indicates which *Device* the Resource refers clothingExpenditure| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
hotelAndRestaurantsExpenditure| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
communicationExpenditure| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
personalCareExpenditure| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
homeAppliancesExpenditure| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
electricityAndGasExpenditure| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
educationExpenditure| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
vacationPackagesExpenditure| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
personalVehicleExpenditure| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
income| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
pensionFoundAndLifeInsurance| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
debts| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
unemployed| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
employed| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
investments| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
houseLoan| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
familyMembers| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
onlinePurchases| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
cinema| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
concerts| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
homeBanking| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
museums| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
theatres| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
personalLoans| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
savings| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
educationalQualification| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"
houseValue| Number|`{0,1,2}`|`0` means "low"; `1` means "medium"; `2` means "high"


## Example Object


```json
{
	"device": {
		"id": "56fa6815af4ecd00010b000a",
		"customId": "My first device",
		"token": "TOKEN"
	},
	"clothingExpenditure": 1,
	"hotelAndRestaurantsExpenditure": 1,
	"communicationExpenditure": 1,
	"personalCareExpenditure": 1,
	"homeAppliancesExpenditure": 1,
	"electricityAndGasExpenditure": 1,
	"educationExpenditure": 1,
	"vacationPackagesExpenditure": 1,
	"personalVehicleExpenditure": 1,
	"income": 1,
	"pensionFoundAndLifeInsurance": 1,
	"debts": 1,
	"unemployed": 1,
	"employed": 1,
	"investments": 1,
	"houseLoan": 1,
	"familyMembers": 1,
	"onlinePurchases": 1,
	"cinema": 1,
	"concerts": 1,
	"homeBanking": 1,
	"museums": 1,
	"theatres": 1,
	"personalLoans": 1,
	"savings": 1,
	"educationalQualification": 1,
	"houseValue": 1
}
```
