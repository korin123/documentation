# Resources


Data Models used to represent [Device-related](/api/concepts/resource-definition.md) Resources.

According to the set of defined [Device-related Resources](/api/reference/resources/resources/platform-created/device-related/index.md), the following Data Models are defined.

* [DetectedPosition](/api/reference/data-modelsata-models/resources/platform-created/device-related/detected-position.md)
* [Visit](/api/reference/data-modelsata-models/resources/platform-created/device-related/visit.md)
* [HomeLocation](/api/reference/data-models/platform-created/resources/device-related/home-location.md)
* [WorkLocation](/api/reference/data-models/platform-created/resources/device-related/work-location.md)
* [Profile](/api/reference/data-modelsata-models/resources/platform-created/device-related/profile.md)
