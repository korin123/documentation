# Platform-created Resources

Data Models used to represent [Platform-created](/api/concepts/resource-definition.md) Resources.

According to the set of defined [Platform-created Resources](/api/reference/resources/resources/platform-created/index.md), the following Data Models are defined.

* [Device](/api/reference/data-models/resources/platform-created/device-related/device.md)
* [Device-related Resources](/api/reference/resources/resources/platform-created/device-related/index.md)
	* [DetectedPosition](/api/reference/data-modelsata-models/resources/platform-created/device-related/detected-position.md)
	* [Activity](/api/reference/data-modelsata-models/resources/platform-created/device-related/activity.md)
	* [Visit](/api/reference/data-modelsata-models/resources/platform-created/device-related/visit.md)
	* [Travel](/api/reference/data-modelsata-models/resources/platform-created/device-related/travel.md)
	* [LivingArea](/api/reference/data-models/platform-created/resources/device-related/living-area.md)
	* [HomeLocation](/api/reference/data-models/platform-created/resources/device-related/home-location.md)
	* [WorkLocation](/api/reference/data-models/platform-created/resources/device-related/work-location.md)
	* [Flight](/api/reference/data-modelsata-models/resources/platform-created/device-related/flight.md)
* [POI](/api/reference/data-modelsata-models/resources/platform-created/poi.md)
