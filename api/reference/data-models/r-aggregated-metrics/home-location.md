# HomeLocation-AggregatedMetrics

Name        |Type      | Description
------------|----------|------------
count | [`HomeLocation-AggregatedMetric`](/api/reference/data-modelsata-models/r-aggregated-metric/home-location.md) | Contains values for the *count* metric.


```json

```
