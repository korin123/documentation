# Travel-AggregatedMetrics

Name        |Type      | Description
------------|----------|------------
count | [`Travel-AggregatedMetric`](/api/reference/data-modelsata-models/r-aggregated-metric/travel.md) | Contains values for the *count* metric.
totalDuration | [`Travel-AggregatedMetric`](/api/reference/data-modelsata-models/r-aggregated-metric/travel.md) | Contains values for the *totalDuration* metric.
averageDuration | [`Travel-AggregatedMetric`](/api/reference/data-modelsata-models/r-aggregated-metric/travel.md) | Contains values for the *averageDuration* metric.
totalTravelledDistance | [`Travel-AggregatedMetric`](/api/reference/data-modelsata-models/r-aggregated-metric/travel.md) | Contains values for the *totalTravelledDistance* metric.
averageTravelledDistance | [`Travel-AggregatedMetric`](/api/reference/data-modelsata-models/r-aggregated-metric/travel.md) | Contains values for the *totalTravelledDistance* metric.
averageStartEndDistance | [`Travel-AggregatedMetric`](/api/reference/data-modelsata-models/r-aggregated-metric/travel.md) | Contains values for the *averageStartEndDistance* metric.
averageStraightIndex | [`Travel-AggregatedMetric`](/api/reference/data-modelsata-models/r-aggregated-metric/travel.md) | Contains values for the *averageStraightIndex* metric.
averageStationaryIndex | [`Travel-AggregatedMetric`](/api/reference/data-modelsata-models/r-aggregated-metric/travel.md) | Contains values for the *averageStationaryIndex* metric.

```json

```
