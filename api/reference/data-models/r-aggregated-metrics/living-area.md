# LivingArea-AggregatedMetrics

Name        |Type      | Description
------------|----------|------------
count | [`LivingArea-AggregatedMetric`](/api/reference/data-models/r-aggregated-metric/trliving-areaavel.md) | Contains values for the *count* metric.


```json

```

