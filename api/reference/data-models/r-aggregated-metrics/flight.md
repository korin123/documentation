# Flight-AggregatedMetrics

Name        |Type      | Description
------------|----------|------------
count | [`Flight-AggregatedMetric`](/api/reference/data-modelsata-models/r-aggregated-metric/flight.md) | Contains values for the *count* metric.
totalDuration | [`Flight-AggregatedMetric`](/api/reference/data-modelsata-models/r-aggregated-metric/flight.md) | Contains values for the *totalDuration* metric.
averageDuration | [`Flight-AggregatedMetric`](/api/reference/data-modelsata-models/r-aggregated-metric/flight.md) | Contains values for the *averageDuration* metric.
totalTravelledDistance | [`Flight-AggregatedMetric`](/api/reference/data-modelsata-models/r-aggregated-metric/flight.md) | Contains values for the *totalTravelledDistance* metric.
averageTravelledDistance | [`Flight-AggregatedMetric`](/api/reference/data-modelsata-models/r-aggregated-metric/flight.md) | Contains values for the *totalTravelledDistance* metric.


```json

```
