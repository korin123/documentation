# Visit-AggregatedMetrics

Name        |Type      | Description
------------|----------|------------
count | [`Visit-AggregatedMetric`](/api/reference/data-modelsata-models/r-aggregated-metric/visit.md) | Contains values for the *count* metric.
totalDuration | [`Visit-AggregatedMetric`](/api/reference/data-modelsata-models/r-aggregated-metric/visit.md) | Contains values for the *totalDuration* metric.
averageDuration | [`Visit-AggregatedMetric`](/api/reference/data-modelsata-models/r-aggregated-metric/visit.md) | Contains values for the *averageDuration* metric.
averageStationaryIndex | [`Visit-AggregatedMetric`](/api/reference/data-modelsata-models/r-aggregated-metric/visit.md) | Contains values for the *averageStationaryIndex* metric.


```json

```
