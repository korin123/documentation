# WorkLocation-AggregatedMetrics

Name        |Type      | Description
------------|----------|------------
count | [`WorkLocation-AggregatedMetric`](/api/reference/data-modelsata-models/r-aggregated-metric/work-location.md) | Contains values for the *count* metric.


```json

```
