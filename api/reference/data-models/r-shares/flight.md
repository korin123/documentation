
# Flight-Shares
Name        |Type      | Description
------------|----------|------------
overall | [`GlobalAndSelection-Shares`](/api/reference/data-models/global-and-selection-share.md) | As defined for a generic [`R-Shares`](/api/reference/data-modelsata-models/r-shares/index.md).
deviceBase | [`Point-SelectionOperationsShares`](/api/reference/data-modelsata-models/g-selection-operation-shares/point.md) | Indicates the Share of a Segment of Flights that comes from each of the possible selection operations on the *Device Base* Dimension
space | [`Sequence-SelectionOperationsShares`](/api/reference/data-modelsata-models/g-selection-operation-shares/sequence.md) | Indicates the Share of a Segment of Flights that comes from each of the possible selection operations on the *Space* Dimension
airports | [`Sequence-SelectionOperationsShares`](/api/reference/data-modelsata-models/g-selection-operation-shares/sequence.md) | Indicates the Share of a Segment of Flights that comes from each of the possible selection operations on the *Airports* Dimension
flightRange | [`Point-SelectionOperationsShares`](/api/reference/data-modelsata-models/g-selection-operation-shares/point.md) | Indicates the Share of a Segment of Flights that comes from each of the possible selection operations on the *Flight Range* Dimension

