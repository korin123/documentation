# Common Data Models

This section provides the set of partial data models that are common to several data models.

* [AdministrativeArea](administrative-area.md)
* [Audience](audience.md)
* [ComparisonCondition](comparison-condition.md)
* [Date](date.md)
* [DeviceReference](device-reference.md)
* [GeoPoint](geo-point.md)
* [GuGeoJSON](gu-geo-json.md)
* [Hook](hook.md)
* [HookMessage](hook-message.md)
* [TargetPOIs](target-pois.md)
* [TimeIntervalDuration](time-interval-duration.md)
