# Flight-AggregatedMetric
Name        |Type      | Description
------------|----------|------------
absolute | `Number` | The absolute value for the aggregated metric.
shares | [`Flight-Shares`](/api/reference/data-modelsata-models/r-shares/flight.md) | Values of the [*Segment-Share*](/api/concepts/statistics.md#share)
