
# HomeLocation-AggregatedMetric
Name        |Type      | Description
------------|----------|------------
absolute | `Number` | The absolute value for the aggregated metric.
shares | [`HomeLocation-Shares`](/api/reference/data-modelsata-models/r-shares/home-location.md) | Values of the [*Segment-Share*](/api/concepts/statistics.md#share)
