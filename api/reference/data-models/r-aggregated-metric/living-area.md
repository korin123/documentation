
# LivingArea-AggregatedMetric
Name        |Type      | Description
------------|----------|------------
absolute | `Number` | The absolute value for the aggregated metric.
shares | [`LivingArea-Shares`](/api/reference/data-modelsata-models/r-shares/living-area.md) | Values of the [*Segment-Share*](/api/concepts/statistics.md#share)
