# Data Models

This section lists the various Data Models used on this API


* [Resources](resources/index.md)
    * [Platform-created](resources/platform-created/index.md)
    * [User-created](resources/user-created/index.md)
* [R-Selection](r-selection/index.md)
* [D-Segment](d-segment/index.md)
* [D-Segmentation](d-segmentation/index.md)
* [D-C-Selection](d-c-selection-operation/index.md)
* [D-C-Segmentation](g-d-segmentation/index.md)
* [R-StatisticInput](r-statistic-input/index.md)
* [R-Segmentation](r-segmentation/index.md)
* [R-RequestedMetrics](r-requested-metrics/index.md)
* [R-AggregatedMetricsCondition](r-aggregated-metrics-condition/index.md)
* [R-StatisticOutput](r-statistic-output/index.md)
* [R-AggregatedMetrics](r-aggregated-metrics/index.md)
* [R-AggregatedMetric](r-aggregated-metric/index.md)
* [R-Shares](r-shares/index.md)
* [G-SelectionOperationShares](g-selection-operation-shares/index.md)
* [Common](common/index.md)
