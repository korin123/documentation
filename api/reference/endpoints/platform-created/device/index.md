* [Documentation Home](../../../../../README.md)
    * [Web API](../../../../index.md)  
      * [Reference](../../../index.md)  
        * [Endpoints](../../index.md)
            * [Resources endpoints](../../index.md)
               * [Platform-created resources endpoints](../index.md)

## Device endpoints

Endpoints for [Device resource](../../../resources/platform-created/device.md) 

[Selection](selection.md)  
[Statistic](statistic.md)