* [Documentation Home](../../../../README.md)
    * [Web API](../../../index.md)  
      * [Reference](../../index.md)  
        * [Endpoints](../index.md)
            
# Platform-created Resource Endpoints

Endpoints for [platform-created resources](../../resources/platform-created/index.md)

* [*Device*](device/index.md)
* [Device-related Resources](device-related/index.md)
    * [*Position*](device-related/position/index.md)
    * [*Visit*](device-related/visit/index.md)
    * [*HomeLocation*](device-related/home-location/index.md)
    * [*WorkLocation*](device-related/work-location/index.md)
    * [*UserProfile*](device-related/user-profile/index.md)
    * [*NightLifeIndex*](device-related/nightlife-index/index.md)
    * [*TrackingSummary*](device-related/tracking-summary/index.md)
* [TriggerLog](triggerlog/index.md)
* [POI](poi/index.md)



 


