* [Documentation Home](../../../../../README.md)
    * [Web API](../../../../index.md)  
      * [Reference](../../../index.md)  
        * [Endpoints](../../index.md)
           * [Platform-created resources endpoints](../index.md)

## POI endpoints

Endpoints for [POI resource](../../../resources/platform-created/poi.md)

* [Selection](selection.md)  
* [Statistic](statistic.md)