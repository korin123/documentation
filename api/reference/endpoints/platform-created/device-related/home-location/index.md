* [Documentation Home](../../../../../../README.md)
    * [Web API](../../../../../index.md)  
      * [Reference](../../../../index.md)  
        * [Endpoints](../../../index.md)
           * [Platform-created resources endpoints](../../index.md)
                * [Device-related resources endpoints](../index.md)
           
## Home Location endpoints

Endpoints for [Home Location resource](../../../../resources/platform-created/device-related/home-location.md)

* [Selection](selection.md)  
* [Statistic](statistic.md)