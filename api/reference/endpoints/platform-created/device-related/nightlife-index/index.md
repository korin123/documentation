* [Documentation Home](../../../../../../README.md)
    * [Web API](../../../../../index.md)  
      * [Reference](../../../../index.md)  
        * [Endpoints](../../../index.md)
           * [Platform-created resources endpoints](../../index.md)
                * [Device-related resources endpoints](../index.md)
           
## Nightlife Index endpoints

Endpoints for [Nightlife Index resource](../../../../resources/platform-created/device-related/nightlife-index.md)

* [Selection](selection.md)  
* [Statistic](statistic.md)