* [Documentation Home](../../../../../README.md)
    * [Web API](../../../../index.md)  
      * [Reference](../../../index.md)  
        * [Endpoints](../../index.md)
            * [Resources endpoints](../../index.md)
               * [Platform-created resources endpoints](../index.md)
               
# Device-related Resource Endpoints

Endpoints for [Device-related resources](../../../resources/platform-created/device-related/index.md).


* [*Position*](position/index.md)
* [*Visit*](visit/index.md)
* [*HomeLocation*](home-location/index.md)
* [*WorkLocation*](work-location/index.md)
* [*UserProfile*](user-profile/index.md)
* [*NightLifeIndex*](nightlife-index/index.md)
* [*TrackingSummary*](tracking-summary/index.md)


 


