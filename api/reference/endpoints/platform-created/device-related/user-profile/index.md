* [Documentation Home](../../../../../../README.md)
    * [Web API](../../../../../index.md)  
      * [Reference](../../../../index.md)  
        * [Endpoints](../../../index.md)
           * [Platform-created resources endpoints](../../index.md)
                * [Device-related resources endpoints](../index.md)
           
## User Profile Index endpoints

Endpoints for [User Profile resource](../../../../resources/platform-created/device-related/profile.md)

* [Selection](selection.md)  
* [Statistic](statistic.md)