* [Documentation Home](../../../../../../README.md)
    * [Web API](../../../../../index.md)  
      * [Reference](../../../../index.md)  
        * [Endpoints](../../../index.md)
           * [Platform-created resources endpoints](../../index.md)
                * [Device-related resources endpoints](../index.md)
           
## Position endpoints

Endpoints for [Position resource](../../../../resources/platform-created/device-related/position.md)

* [Selection](selection.md)  
* [Statistic](statistic.md)