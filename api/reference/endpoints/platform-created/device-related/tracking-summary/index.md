* [Documentation Home](../../../../../../README.md)
    * [Web API](../../../../../index.md)  
      * [Reference](../../../../index.md)  
        * [Endpoints](../../../index.md)
           * [Platform-created resources endpoints](../../index.md)
                * [Device-related resources endpoints](../index.md)
           
## Tracking Summary endpoints

Endpoints for [Tracking Summary resource](../../../../resources/platform-created/device-related/tracking-summary.md)

* [Selection](selection.md)  
* [Statistic](statistic.md)