* [Documentation Home](../../../../../../README.md)
    * [Web API](../../../../../index.md)  
      * [Reference](../../../../index.md)  
        * [Endpoints](../../../index.md)
           * [Platform-created resources endpoints](../../index.md)
                * [Device-related resources endpoints](../index.md)
           
## Visit endpoints

Endpoints for [Visit resource](../../../../resources/platform-created/device-related/visit.md)

* [Selection](selection.md)  
* [Statistic](statistic.md)