* [Documentation Home](../../../../../../README.md)
    * [Web API](../../../../../index.md)  
      * [Reference](../../../../index.md)  
        * [Endpoints](../../../index.md)
           * [Platform-created resources endpoints](../../index.md)
                * [Device-related resources endpoints](../index.md)
           
## Work Location endpoints

Endpoints for [Work Location resource](../../../../resources/platform-created/device-related/work-location.md)

* [Selection](selection.md)  
* [Statistic](statistic.md)