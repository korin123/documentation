* [Documentation Home](../../../../../../README.md)  
  * [Web API](../../../../../index.md)  
    * [Reference](../../../../index.md)
        * [Endpoints](../../../index.md)

# Trigger Endpoints

Endpoints to perform CRUD operations on [*Trigger* Resources](../../../../resources/user-created/trigger.md)

* **[CREATE Resource](create.md)**
* **[READ Collection](read-collection.md)**
* **[READ single Resource](read-resource.md)**
* **[UPDATE Resource](update.md)**
* **[DELETE Resource](delete.md)**
