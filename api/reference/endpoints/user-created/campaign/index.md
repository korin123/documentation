# Campaign Endpoints

Endpoints to perform CRUD operations on [*Campaign* Resources](/api/reference/resources/resources/user-created/campaign.md)

* **[CREATE Resource](/api/reference/endpoints/endpoints/resources/user-created/campaign/create.md)**
* **[READ Collection](/api/reference/endpoints/endpoints/resources/user-created/campaign/read-collection.md)**
* **[READ single Resource](/api/reference/endpoints/endpoints/resources/user-created/campaign/read-resource.md)**
* **[UPDATE Resource](/api/reference/endpoints/endpoints/resources/user-created/campaign/update.md)**
* **[DELETE Resource](/api/reference/endpoints/resources/user-created/campaign/delete.md)**
