# Footfall Analysis Endpoints

Endpoints to perform CRUD operations on [*Footfall Analysis* Resources](/api/reference/resources/resources/user-created/campaign-footfall.md)

* **[CREATE Resource](/api/reference/endpoints/endpoints/resources/user-created/campaign/footfall/create.md)**
* **[READ Collection](/api/reference/endpoints/endpoints/resources/user-created/campaign/footfall/read-collection.md)**
* **[READ single Resource](/api/reference/endpoints/endpoints/resources/user-created/campaign/footfall/read-resource.md)**
* **[Download results](/api/reference/endpoints/endpoints/resources/user-created/campaign/footfall/download-results.md)**