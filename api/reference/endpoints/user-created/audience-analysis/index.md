# Audience Analysis Endpoints

Endpoints to perform CRUD operations on [*Audience Analysis* Resources](/api/reference/resources/resources/user-created/audience-analysis.md)

* **[CREATE Resource](/api/reference/endpoints/endpoints/resources/user-created/audience-analysis/create.md)**
* **[READ Collection](/api/reference/endpoints/endpoints/resources/user-created/audience-analysis/read-collection.md)**
* **[READ single Resource](/api/reference/endpoints/endpoints/resources/user-created/audience-analysis/read-resource.md)**
* **[Download results](/api/reference/endpoints/endpoints/resources/user-created/audience-analysis/download-results.md)**