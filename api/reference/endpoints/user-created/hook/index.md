# Hook Endpoints

Endpoints to perform CRUD operations on [*Hook* Resources](/api/reference/resources/user-created/hook.md)

* **[CREATE Resource](/api/reference/endpoints/endpoints/resources/user-created/hook/create.md)**
* **[READ Collection](/api/reference/endpoints/endpoints/resources/user-created/hook/read-collection.md)**
* **[READ single Resource](/api/reference/endpoints/endpoints/resources/user-created/hook/read-resource.md)**
* **[UPDATE Resource](/api/reference/endpoints/endpoints/resources/user-created/hook/update.md)**
* **[DELETE Resource](/api/reference/endpoints/endpoints/resources/user-created/hook/delete.md)**
