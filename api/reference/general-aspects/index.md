* [Documentation Home](../../../README.md)  
  * [Web API](../../index.md)
    * [Reference](../../index.md)
  
# General Aspects

This section deals with the general aspects concerning this API. Specifically, the following aspects are covered.

* **[Notation](notation.md)**. Deals with notation conventions used by this API.
* **[Content Type](content-type.md)**. Deals with the content type used by this API.
* **[Pagination](pagination.md)**. Explains how pagination is used by this API and how it can be controlled when performing requests.
* **[Authentication](auth.md)**. Documents the authentication mechanisms used on this API.