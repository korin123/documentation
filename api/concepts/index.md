* [Documentation Home](../../README.md)
   * [Web API](../index.md)

# Basic Concepts and Functionalities

This section describes the basic concepts under GeoUniq Analytics API.

* [Resources, Dimensions, and Configurations](resource-definition.md)
* [Selection of Resources](resource-selection.md)
* [Statistics](statistics.md)
