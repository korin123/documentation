# GU Analytics Platform documentation.

* [Service Architecture](service-architecture.md)
* [Getting Started With Integration](getting-started.md)
* [Web API](api/index.md)
* [SDK](sdk/index.md)
