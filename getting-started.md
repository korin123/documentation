* [Documentation Home](README.md)  

# Getting started with Geouniq integration

To get started with GeoUniq, follow the steps below

1. Create and configure a Project, as described [here](project-configuration.md)
2. Integrate the SDK into your mobile app, as described [here](sdk/integration/index.md)


[Next: Project Configuration](project-configuration.md)  


